import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LookUpCaseCmpComponent } from './look-up-case-cmp.component';

describe('LookUpCaseCmpComponent', () => {
  let component: LookUpCaseCmpComponent;
  let fixture: ComponentFixture<LookUpCaseCmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LookUpCaseCmpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LookUpCaseCmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
