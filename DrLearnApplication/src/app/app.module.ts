import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponentComponent } from './home-page-component/home-page-component.component';
import { HeaderCmpComponent } from './header-cmp/header-cmp.component';
import { NavbarCmpComponent } from './navbar-cmp/navbar-cmp.component';
import { LookUpCaseCmpComponent } from './look-up-case-cmp/look-up-case-cmp.component';
import { AddCaseCmpComponent } from './add-case-cmp/add-case-cmp.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponentComponent,
    HeaderCmpComponent,
    NavbarCmpComponent,
    LookUpCaseCmpComponent,
    AddCaseCmpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
