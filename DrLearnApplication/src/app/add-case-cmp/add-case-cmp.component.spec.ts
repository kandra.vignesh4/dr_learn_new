import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCaseCmpComponent } from './add-case-cmp.component';

describe('AddCaseCmpComponent', () => {
  let component: AddCaseCmpComponent;
  let fixture: ComponentFixture<AddCaseCmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCaseCmpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCaseCmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
